import React, { Component } from "react";
import PostDataService from "../services/post.service";

import PostForm from "./PostForm";

export default class PostList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: [],
      currentPost: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    PostDataService.getAll().on("value", this.onDataChange);
  }

  componentWillUnmount() {
    PostDataService.getAll().off("value", this.onDataChange);
  }

  onDataChange = (items) => {
    let posts = [];

    items.forEach((item) => {
      let key = item.key;
      let data = item.val();
      posts.push({
        key: key,
        title: data.title,
        description: data.description,
        published: data.published,
      });
    });

    this.setState({
        posts: posts,
    });
  }

  refreshList = () => {
    this.setState({
      currentPost: null,
      currentIndex: -1,
    });
  }

  setActivePost = (post, index) => {
    this.setState({
        currentPost: post,
        currentIndex: index,
    });
  }

  removeAllPosts = () => {
    PostDataService.deleteAll()
      .then(() => {
        this.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { posts, currentPost, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-6">
          <h4>Post List</h4>

          <ul className="list-group">
            {posts &&
              posts.map((post, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActivePost(post, index)}
                  key={index}
                >
                  {post.title}
                </li>
              ))}
          </ul>

          <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllPosts}
          >
            Remove All
          </button>
        </div>
        <div className="col-md-6">
          {currentPost ? (
            <PostForm
              post={currentPost}
              refreshList={this.refreshList}
            />
          ) : (
            <div>
              <br />
              <p>Please click on a Post...</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}
