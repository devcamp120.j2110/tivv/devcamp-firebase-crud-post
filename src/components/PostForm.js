import React, { Component } from "react";
import PostDataService from "../services/post.service";

export default class PostForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPost: {
        key: null,
        title: "",
        description: "",
        published: false,
      },
      message: "",
    };
  }

  static getDerivedStateFromProps = (nextProps, prevState) => {
    const { post } = nextProps;
    if (prevState.currentPost.key !== post.key) {
      return {
        currentPost: post,
        message: ""
      };
    }

    return prevState.currentPost;
  }

  componentDidMount() {
    this.setState({
        currentPost: this.props.post,
    });
  }

  onChangeTitle = (e) => {
    const title = e.target.value;

    this.setState(function (prevState) {
      return {
        currentPost: {
          ...prevState.currentPost,
          title: title,
        },
      };
    });
  }

  onChangeDescription= (e) => {
    const description = e.target.value;

    this.setState((prevState) => ({
        currentPost: {
        ...prevState.currentPost,
        description: description,
      },
    }));
  }

  updatePublished = (status) => {
    PostDataService.update(this.state.currentPost.key, {
      published: status,
    })
      .then(() => {
        this.setState((prevState) => ({
          currentPost: {
            ...prevState.currentPost,
            published: status,
          },
          message: "The status was updated successfully!",
        }));
      })
      .catch((e) => {
        console.log(e);
      });
  }

  updatePost = () => {
    const data = {
      title: this.state.currentPost.title,
      description: this.state.currentPost.description,
    };

    PostDataService.update(this.state.currentPost.key, data)
      .then(() => {
        this.setState({
          message: "The post was updated successfully!",
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  deletePost = () => {
    PostDataService.delete(this.state.currentPost.key)
      .then(() => {
        this.props.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentPost } = this.state;

    return (
      <div>
        <h4>Post</h4>
        {currentPost ? (
          <div className="edit-form">
            <form>
              <div className="form-group">
                <label htmlFor="title">Title</label>
                <input
                  type="text"
                  className="form-control"
                  id="title"
                  value={currentPost.title}
                  onChange={this.onChangeTitle}
                />
              </div>
              <div className="form-group">
                <label htmlFor="description">Description</label>
                <input
                  type="text"
                  className="form-control"
                  id="description"
                  value={currentPost.description}
                  onChange={this.onChangeDescription}
                />
              </div>

              <div className="form-group">
                <label>
                  <strong>Status:</strong>
                </label>
                {currentPost.published ? "Published" : "Pending"}
              </div>
            </form>

            {currentPost.published ? (
              <button
                className="badge badge-primary mr-2"
                onClick={() => this.updatePublished(false)}
              >
                UnPublish
              </button>
            ) : (
              <button
                className="badge badge-primary mr-2"
                onClick={() => this.updatePublished(true)}
              >
                Publish
              </button>
            )}

            <button
              className="badge badge-danger mr-2"
              onClick={this.deletePost}
            >
              Delete
            </button>

            <button
              type="submit"
              className="badge badge-success"
              onClick={this.updatePost}
            >
              Update
            </button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Post...</p>
          </div>
        )}
      </div>
    );
  }
}
