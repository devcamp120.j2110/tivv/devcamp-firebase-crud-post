import * as firebase from "firebase";
import "firebase/database";

let config = {
    apiKey: "AIzaSyDSqwl7dy_upxLbCnayuMTPfYGRPZZb_Xg",
    authDomain: "devcamp-firebase-ac0f9.firebaseapp.com",
    projectId: "devcamp-firebase-ac0f9",
    storageBucket: "devcamp-firebase-ac0f9.appspot.com",
    messagingSenderId: "999489926085",
    appId: "1:999489926085:web:df71855e65d64d0cfd91ac",
    databaseURL: "https://devcamp-firebase-ac0f9-default-rtdb.firebaseio.com/",
};

firebase.initializeApp(config);

export default firebase.database();
